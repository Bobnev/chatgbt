import org.junit.jupiter.api.Test;

import java.io.FileNotFoundException;
import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class SalesAnalyticsTest {

    @Test
    public void testReadFromCSV() throws Exception {
        SalesAnalytics cut = new SalesAnalytics();
        cut.readFromCSV("src/test/resources/sales_ok.csv");
        assertEquals(7, cut.getSaleList().size());
    }

    @Test
    public void testReadFromNonExistentCSVThrows() throws FileNotFoundException {
        SalesAnalytics cut = new SalesAnalytics();
        assertThrows(Exception.class,() -> cut.readFromCSV("src/test/resources/not_existent.csv"));
    }

    @Test
    public void testReadFromBadCSV() throws Exception {
        SalesAnalytics cut = new SalesAnalytics();
        cut.readFromCSV("src/test/resources/sales_bad.csv");
        assertEquals(0, cut.getSaleList().size());
    }

    @Test
    public void testGetSalesOfEmployee()  {
        SalesAnalytics cut = new SalesAnalytics();
        cut.setSaleList(SalesData.sales);
        assertEquals(2, cut.getSalesOfEmployee().size());
    }

    @Test
    public void testGetSalesOfEmployeeValues() {
        SalesAnalytics cut = new SalesAnalytics();
        cut.setSaleList(SalesData.sales);
        assertEquals(4, cut.getSalesOfEmployee().get("Werner").size());
    }

    @Test
    public void testGetSalesPerDayDays() {
        SalesAnalytics cut = new SalesAnalytics();
        cut.setSaleList(SalesData.sales);
        assertEquals(3, cut.getSalesPerDay(LocalDate.of(2022, 11, 27),
                LocalDate.of(2022, 11, 30)).size());

    }

    @Test
    public void testGetSalesPerDay() {
        SalesAnalytics cut = new SalesAnalytics();
        cut.setSaleList(SalesData.sales);
        assertEquals(2, cut.getSalesPerDay(LocalDate.of(2022, 11, 27),
                LocalDate.of(2022, 11, 30))
                .get(LocalDate.of(2022, 11, 27)).size());
    }
    @Test
    public void testGetSalesPerDayOneDay() {
        SalesAnalytics cut = new SalesAnalytics();
        cut.setSaleList(SalesData.sales);
        assertEquals(2, cut.getSalesPerDay(LocalDate.of(2022, 11, 27),
                        LocalDate.of(2022, 11, 30))
                .get(LocalDate.of(2022, 11, 27)).size());
    }

    @Test
    public void filterSalesEmployeeAndBrand() {
        SalesAnalytics cut = new SalesAnalytics();
        cut.setSaleList(SalesData.sales);
        assertEquals(1, cut.filterSellerAndBrand("Werner","Steyr").size());
    }

}
