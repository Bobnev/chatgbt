import java.time.LocalDate;

public class Sale {
    private String employee;
    private LocalDate date;
    private double price;
    private String productCategory;
    private String brand;

    public Sale(LocalDate date, double price, String employee, String productCategory, String brand) {
        if (employee == null || date == null || productCategory == null || brand == null) {
            throw new IllegalArgumentException("Invalid arguments for Sale constructor");
        }
        this.employee = employee;
        this.date = date;
        this.price = price;
        this.productCategory = productCategory;
        this.brand = brand;
    }

    public String getEmployee() {
        return employee;
    }

    public LocalDate getDate() {
        return date;
    }

    public double getPrice() {
        return price;
    }

    public String getProductCategory() {
        return productCategory;
    }

    public String getBrand() {
        return brand;
    }

    public static Sale ofCSVString(String csv) {
        String[] fields = csv.split(",");
        if (fields.length != 5) {
            throw new IllegalArgumentException("Invalid CSV string for Sale");
        }
        LocalDate date = LocalDate.parse(fields[1]);
        double price = Double.parseDouble(fields[2]);
        String employee = fields[0];
        String productCategory = fields[3];
        String brand = fields[4];
        return new Sale(date, price, employee, productCategory, brand);
    }
}
