import java.io.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.function.Predicate;

public class SalesAnalytics {

    private List<Sale> saleList;
    private int numErrors;

    public SalesAnalytics() {
        this.saleList = new ArrayList<>();
        this.numErrors = 0;
    }

    public void readFromCSV(String filename) throws Exception {
        List<Sale> sales = new ArrayList<>();
        int numErrors = 0;
        try (BufferedReader br = new BufferedReader(new FileReader(filename))) {
            // Skip the first line
            br.readLine();
            String line;
            while ((line = br.readLine()) != null) {
                try {
                    Sale sale = Sale.ofCSVString(line);
                    sales.add(sale);
                } catch (Exception e) {
                    numErrors++;
                    System.err.println("Invalid sale: " + e.getMessage());
                }
            }
            saleList = sales;
        } catch (Exception e) {
            numErrors++;

            throw e;
        }
    }



    public Map<String, List<Sale>> getSalesOfEmployee() {
        Map<String, List<Sale>> salesByEmployee = new HashMap<>();
        for (Sale sale : this.saleList) {
            String employee = sale.getEmployee();
            if (!salesByEmployee.containsKey(employee)) {
                salesByEmployee.put(employee, new ArrayList<>());
            }
            salesByEmployee.get(employee).add(sale);
        }
        return salesByEmployee;
    }

    public SortedMap<LocalDate, List<Sale>> getSalesPerDay(LocalDate from, LocalDate to) {
        SortedMap<LocalDate, List<Sale>> salesByDay = new TreeMap<>();
        for (Sale sale : this.saleList) {
            LocalDate date = sale.getDate();
            if (date.compareTo(from) >= 0 && date.compareTo(to) < 0) {
                if (!salesByDay.containsKey(date)) {
                    salesByDay.put(date, new ArrayList<>());
                }
                salesByDay.get(date).add(sale);
            }
        }
        return salesByDay;
    }

    public List<Sale> filter(Predicate<Sale> filter) {
        List<Sale> filteredList = new ArrayList<>();
        for (Sale sale : this.saleList) {
            if (filter.test(sale)) {
                filteredList.add(sale);
            }
        }
        return filteredList;
    }

    public List<Sale> filterSellerAndBrand(String employee, String brand) {
        Predicate<Sale> filter = sale -> sale.getEmployee().equals(employee) && sale.getBrand().equals(brand);
        return filter(filter);
    }

    public static <T> void printSales(Map<T, List<Sale>> sales) {
        for (T key : sales.keySet()) {
            System.out.println(key + ":");
            for (Sale sale : sales.get(key)) {
                System.out.println("\t" + sale);
            }
        }
    }

    public List<Sale> getSaleList() {
        return this.saleList;
    }

    public void setSaleList(List<Sale> sales) {
        this.saleList=sales;
    }
}